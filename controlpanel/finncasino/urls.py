# finncasino/urls.py
from django.conf.urls import url
from finncasino import views
app_name = 'finncasino'

urlpatterns=[
    url(r'^register/$',views.register,name='register'),
    url(r'^user_login/$',views.user_login,name='user_login'),
    url(r'^events/$',views.events,name='events'),
    url(r'^events/(?P<event_id>\d+)/$',views.events,name='events'),
]