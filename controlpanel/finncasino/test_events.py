from datetime import timedelta
from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth.models import User
from finncasino.models import Event

from finncasino.models import Event

class TestView(TestCase):
  
  def setUp(self):
    self.client = Client()
    user = User(username="Tester")
    user.set_password("hunter1")
    user.save()

    event = Event(
      name="European Championships league match | England v Russia",
      participantA="England",
      participantB="Russia",
      oddToWin=1.2,
      oddToDraw=1.7,
      oddToLose=2.3,
      start = timezone.now() + timedelta(days=3),
      end = timezone.now() + timedelta(days=3) + timedelta(hours=2)
    )

    event.save()

  def test_events_GET(self):
    self.client.force_login(User.objects.get_or_create(username='Tester')[0])
    response = self.client.get(reverse('finncasino:events'))
    self.assertEquals(response.status_code, 200)
    self.assertTemplateUsed(response, 'finncasino/events.html')
    self.assertEquals(list(response.context['events']), list(Event.objects.all()))
  
  def test_events_POST(self):
    self.client.force_login(User.objects.get_or_create(username='Tester')[0])
    startLength = len(Event.objects.all())
    data = {
      'name': "European Championships league match | Belgium v Germany",
      'participantA': "Belgium",
      'participantB': "Germany",
      'oddToWin': 1.2,
      'oddToDraw': 1.7,
      'oddToLose': 2.3,
      'start': '12/10/2019 20:51',
      'end': '12/10/2019 22:51'
    }
    response = self.client.post(reverse('finncasino:events'), data)

    self.assertEquals(response.status_code, 200)
    self.assertTemplateUsed(response, 'finncasino/events.html')
    self.assertNotEquals(len(Event.objects.all()), startLength)
    self.assertEquals(len(Event.objects.all()), startLength + 1)

  def test_events_UPDATE(self):
    self.client.force_login(User.objects.get_or_create(username='Tester')[0])
    startLength = len(Event.objects.all())
    event = Event.objects.all().first()
    data = {
      'name': "European Championships league match | Belgium v Germany",
      'participantA': "Belgium",
      'participantB': "Germany",
      'oddToWin': 1.2,
      'oddToDraw': 1.7,
      'oddToLose': 2.3,
      'start': '12/10/2019 20:51',
      'end': '12/10/2019 22:51'
    }
    response = self.client.post(reverse('finncasino:events', kwargs={'event_id':event.id}), data)

    self.assertEquals(response.status_code, 200)
    self.assertTemplateUsed(response, 'finncasino/event.html')
    self.assertEquals(len(Event.objects.all()), startLength)

  def test_events_DELETE(self):
    self.client.force_login(User.objects.get_or_create(username='Tester')[0])
    startLength = len(Event.objects.all())
    response = self.client.delete(reverse('finncasino:events', kwargs={'event_id': Event.objects.all().first().id}))

    self.assertEquals(response.status_code, 200)
    self.assertTemplateUsed(response, 'finncasino/events.html')
    self.assertNotEquals(len(Event.objects.all()), startLength)
    self.assertEquals(len(Event.objects.all()), startLength - 1)