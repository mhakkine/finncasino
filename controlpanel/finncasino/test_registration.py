from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.models import User

class TestView(TestCase):
  def setUp(self):
    self.client = Client()
    user = User(username="Tester", email="hunter@gmail.com")
    user.set_password("hunter1")
    user.save()

  def test_register_GET(self):
    client = Client()
    response = self.client.get(reverse('finncasino:register'))
    self.assertEquals(response.status_code, 200)
    self.assertTemplateUsed(response, 'finncasino/registration.html')

  def test_register_POST_success(self):
    data = {
        'username': "Tester2",
        'email': "hunter2@gmail.com",
        'password': "hunter2"
      }
    response = self.client.post(reverse('finncasino:register'), data)
    self.assertEquals(response.status_code, 200)
    self.assertTemplateUsed(response, 'finncasino/registration.html')
    self.assertEquals(response.context['registered'], True)


  def test_register_POST_fail(self):
    data = {
        'username': "Tester",
        'email': "hunter@gmail.com",
        'password': "hunter"
      }
    response = self.client.post(reverse('finncasino:register'), data)
    self.assertEquals(response.status_code, 200)
    self.assertTemplateUsed(response, 'finncasino/registration.html')
    self.assertEquals(response.context['registered'], False)
