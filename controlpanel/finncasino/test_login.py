from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.models import User

class TestView(TestCase):
  
  def setUp(self):
    self.client = Client()
    user = User(username="Tester")
    user.set_password("hunter1")
    user.save()

  def test_login_GET(self):
    response = self.client.get(reverse('finncasino:user_login'))
    self.assertEquals(response.status_code, 200)
    self.assertTemplateUsed(response, 'finncasino/login.html')

  def test_login_POST_success(self):
    username = "Tester"
    password = "hunter1"
    user = User(username=username, password=password)
    data = {
        'username': user.username,
        'password': user.password
      }
    response = self.client.post(reverse('finncasino:user_login'), data)
    self.assertEquals(response.status_code, 302)

  def test_login_POST_fail(self):
    username = "Tester"
    password = "hunter12"
    user = User(username=username, password=password)
    data = {
        'username': user.username,
        'password': user.password
      }
    response = self.client.post(reverse('finncasino:user_login'), data)
    self.assertEquals(response.status_code, 200)
    self.assertEquals(response.context['error'], "Invalid credentials")
    self.assertTemplateUsed(response, 'finncasino/login.html')