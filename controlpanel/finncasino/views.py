# finncasino/views.py
import datetime
from django.shortcuts import render
from finncasino.forms import EventForm, UserForm
from finncasino.models import Event
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404

def index(request):
    return render(request,'finncasino/index.html')

@login_required
def special(request):
    return HttpResponse("You are logged in !")

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))

def register(request):
    context = {
        "error": "",
        "registered": False
    }

    if request.method == 'POST':
        context["user_form"] = UserForm(data=request.POST)
        if context["user_form"].is_valid():
            user = context["user_form"].save()
            user.set_password(user.password)
            user.save()
            context["registered"] = True
        else:
            context["error"] = context["user_form"].errors
    else:
        context["user_form"] = UserForm()
    return render(request,'finncasino/registration.html',context)

def user_login(request):
    User = get_user_model()

    context = {
        "error": ""
    }
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect(reverse('index'))
            else:
                context["error"] = "Your account is inactive."
        else:
            context["error"]="Invalid credentials"
            return render(request, 'finncasino/login.html', context)
    else:
        return render(request, 'finncasino/login.html', context)

@login_required
def events(request, event_id=None):
    context = {
        "events": Event.objects.all()
    }

    if request.method == 'POST':
        return handle_post(context, request, event_id)

    elif request.method == 'DELETE' and event_id != None:
        return handle_delete(context, request, event_id)

    elif request.method == 'GET':
        return handle_get(context, request, event_id)

    else:
        return handle_unsupported(request)


# POST
def handle_post(context, request, event_id):
    context["event_form"] = EventForm(data=request.POST)

    if context["event_form"].is_valid():
        event = context["event_form"].save(commit=False)

        Event.objects.update_or_create(
            id=event_id,
            defaults={
                'name': event.name,
                'participantA': event.participantA,
                'participantB': event.participantB,
                'start': event.start,
                'end': event.end,
                'oddToWin': event.oddToWin,
                'oddToLose': event.oddToLose,
                'oddToDraw': event.oddToDraw
            }
        )
    else:
        context["error"] = context["event_form"].errors

    if event_id is None:
        return render(request, 'finncasino/events.html', context)
    else:
        return render_event(context, request, event_id)


# DELETE
def handle_delete(context, request, event_id):
    context["event_form"] = EventForm()
    Event.objects.get(id=event_id).delete()

    return render(request, 'finncasino/events.html', context)


# GET
def handle_get(context, request, event_id):
    context["event_form"] = EventForm()

    if event_id is None:
        return render(request, 'finncasino/events.html', context)
    else:
        return render_event(context, request, event_id)


# UNSUPPORTED
def handle_unsupported(request):
    return render(request, 'finncasino/error.html')


# GENERIC
def render_event(context, request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    event.start = event.start.strftime('%d/%m/%Y %H:%M')
    event.end = event.end.strftime('%d/%m/%Y %H:%M')

    context["event_form"] = EventForm(instance=event)

    event = get_object_or_404(Event, pk=event_id)
    context["event"] = event

    return render(request, 'finncasino/event.html', context)
