from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator
from django.core.validators import MinValueValidator
from django.db.models import CharField, DateTimeField, Model, FloatField


def __str__(self):
  return self.user.username

class Event(Model):
  name = CharField(max_length=255)
  participantA = CharField(max_length=255)
  participantB = CharField(max_length=255)
  start = DateTimeField()
  end = DateTimeField()
  oddToWin =  FloatField(validators=[MinValueValidator(1.0), MaxValueValidator(50)])
  oddToLose =  FloatField(validators=[MinValueValidator(1.0), MaxValueValidator(50)])
  oddToDraw =  FloatField(validators=[MinValueValidator(1.0), MaxValueValidator(50)])
  created = DateTimeField(auto_now_add=True)
  modified = DateTimeField(auto_now=True)
