# finncasino/forms.py
from django import forms
from django.contrib.auth.models import User

from finncasino.models import Event
class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ('username','password','email')


class EventForm(forms.ModelForm):
    start = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'])
    end = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'])

    class Meta():
        model = Event
        fields = ('name', 'participantA', 'participantB', 'start','end','oddToWin','oddToLose','oddToDraw')