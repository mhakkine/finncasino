from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.models import User

class TestView(TestCase):
  def test_homepage_GET(self):
    client = Client()
    response = client.get(reverse('index'))
    self.assertEquals(response.status_code, 200)
    self.assertTemplateUsed(response, 'finncasino/index.html')